#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\[\033[1;93m\]$(date +%FT%T%z) ${HOSTNAME}:${0}:${LINENO}\n+ \e[m\]'
else
    PS4='\[\033[1;93m\]$(date +%FT%T%z) ${0}:${LINENO}\n+ \e[m\]'
fi

. /etc/os-release

set -eux


##########################
##                      ##
##  SET UP TESTING ENV  ##
##                      ##
##########################

case $ID in
debian|ubuntu)
    case $VERSION_CODENAME in
    bullseye|bookworm|focal|jammy|noble)
        CODENAME=$VERSION_CODENAME
        ;;
    *)
        echo "ERROR: VERSION_CODENAME not found: $VERSION_CODENAME"
        exit 1
        ;;
    esac
    ;;
*)
    echo "ERROR: ID not found: $ID"
    exit 1
    ;;
esac


################
##            ##
##  DO TESTS  ##
##            ##
################


# TEST: binary (static linked)
./dist/x86_64/alpine/$PKG_NAME --version | grep "^$PKG_VERSION$"

./dist/i686/alpine/$PKG_NAME --version | grep "^$PKG_VERSION$"

# TEST: binary (dynamic linked)
case $CODENAME in
bullseye|focal)
    ./dist/x86_64/bullseye/$PKG_NAME --version | grep "^$PKG_VERSION$"
    ;;
bookworm|jammy|noble)
    ./dist/x86_64/bullseye/$PKG_NAME --version | grep "^$PKG_VERSION$"
    ./dist/x86_64/bookworm/$PKG_NAME --version | grep "^$PKG_VERSION$"
    ;;
*)
    echo "ERROR: CODENAME not found: $CODENAME"
    exit 1
    ;;
esac

# TEST: install .deb package
apt-get install -y ./dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~${CODENAME}_amd64.deb
/usr/bin/$PKG_NAME --version | grep "^$PKG_VERSION$"

# TEST: uninstall .deb package
apt-get autoremove --purge -y $PKG_NAME
test ! -x /usr/bin/$PKG_NAME

# TEST: upgrade .deb package
apt-get install -y ./test_data/${PKG_NAME}_1.43.0-1-xdeb~${CODENAME}_amd64.deb
/usr/bin/$PKG_NAME --version | grep "^1.43.0$"
apt-get install -y ./dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~${CODENAME}_amd64.deb
/usr/bin/$PKG_NAME --version | grep "^$PKG_VERSION$"


exit 0
